-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Oct 24, 2018 at 06:34 PM
-- Server version: 5.6.21
-- PHP Version: 5.5.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `phpmvc`
--

-- --------------------------------------------------------

--
-- Table structure for table `mobil`
--

CREATE TABLE IF NOT EXISTS `mobil` (
`id` int(11) NOT NULL,
  `manufaktur` varchar(50) NOT NULL,
  `tipe` varchar(50) NOT NULL,
  `harga` int(15) NOT NULL,
  `tahun` int(4) NOT NULL,
  `img` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mobil`
--

INSERT INTO `mobil` (`id`, `manufaktur`, `tipe`, `harga`, `tahun`, `img`) VALUES
(1, 'Honda', 'Civic Sport', 321300000, 2018, 'MY19-civic-sedan-sport-ext-jellybean-1x.png'),
(2, 'Honda', 'CR-V Touring', 486000000, 2018, 'crvtouring.jpg'),
(4, 'Honda', 'CR-V EX-L', 440400000, 2017, '2017-cr-v-ex-ext.png'),
(5, 'Honda', 'Clarity Plug-In Hybrid', 501300000, 2017, 'plugin-hybrid.png'),
(6, 'Honda', 'Clarity Plug-In Hybrid', 440400000, 2018, 'plugin-hybrid.png');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `mobil`
--
ALTER TABLE `mobil`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `mobil`
--
ALTER TABLE `mobil`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
