<?php 

class Mobil_model{

	private $table = 'mobil';
	private $db;

	public function __construct(){
		$this->db = new Database;
	}

	public function getAllMobil(){
		$this->db->query('SELECT * FROM ' . $this->table);
		return $this->db->resultSet();
	}

	public function getMobilById($id){
		$this->db->query('SELECT * FROM '.$this->table.' WHERE id=:id');
		$this->db->bind('id', $id);
		return $this->db->single();
	}

	public function insertMobil($data =[]){
		$this->db->query('INSERT INTO '.$this->table.' VALUES(:manufaktur, :tipe, :harga, :tahun, :img)');
		$this->db->bind("manufaktur", $data["manufaktur"]);
		$this->db->bind("tipe", $data["tipe"]);
		$this->db->bind("harga", $data["harga"]);
		$this->db->bind("tahun", $data["tahun"]);
		$this->db->bind("img", $data["img"]);
		$this->db->execute();
	}

	public function deleteById($id){
		$this->db->query('DELETE FROM '.$this->table.'WHERE id=:id');
		$this->db->bind('id', $id);
		$this->db->execute();
	}
}