<div class="container mt-5">
	<div class="row">
		<div class="col-12">
			<h3>Daftar Mobil</h3>
			<a href="" class="badge badge-danger">Tambah</a>
			<hr>
			<div class="card-deck">
				<?php foreach($data['mobil'] as $mobil ) : ?>
					
					<div class="card" style="width: 18rem;">
						<img class="card-img-top" src="<?=BASEURL; ?>/img/<?=$mobil['img']; ?>" alt="Card image cap">
						<div class="card-body">
							<h6 class="card-title"><a href="<?= BASEURL;?>/mobil/detail/<?= $mobil['id']; ?>"><?= $mobil['tipe']; ?></a></h6>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
							tempor incididunt ut labore et dolore magna aliqua.</p>
							<a class="badge badge-danger" href="<?=BASEURL;?>/mobil/hapus/<?=$mobil['id']; ?>">Hapus</a>
							<a class="badge badge-warning" href="<?=BASEURL;?>/mobil/ubah/<?=$mobil['id']; ?>">Ubah</a>
						</div>
					</div>

				<?php endforeach; ?>	
			</div>		
		</div>
	</div>
</div>