<div class="container mt-5">
	<div class="card" style="width: 18rem;">
		<img class="card-img-top" src="<?=BASEURL; ?>/img/<?=$data['mobil']['img']; ?>" alt="Card image cap">
		<div class="card-body">
		    <h5 class="card-title"><?= $data['mobil']['tipe']; ?> <?= $data['mobil']['tahun']; ?></h5>
		    <h6 class="card-subtitle mb-2 text-muted"><?= $data['mobil']['manufaktur']; ?></h6>
		    <p class="card-text">Rp.<?= $data['mobil']['harga']; ?></p>
		    <a href="<?= BASEURL; ?>/mobil" class="card-link">Kembali</a>
		</div>
	</div>
</div>