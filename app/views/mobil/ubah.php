<div class="container mt-5 mx-auto">
	<div class="card" style="width: 23rem;">
		<img class="card-img-top" src="<?=BASEURL; ?>/img/<?=$data['mobil']['img']; ?>" alt="Card image cap">
		<div class="card-body">
		    <input placeholder="Manufaktur" type="text" name="manufaktur" class="form-control mb-2" value="<?=$data['mobil']['manufaktur']; ?>">
		    <input placeholder="Tipe" type="text" name="tipe" class="form-control mb-2" value="<?=$data['mobil']['tipe']; ?>">
		    <input placeholder="Tahun" type="text" name="tahun" class="form-control mb-2" value="<?=$data['mobil']['tahun']; ?>">
		    <input placeholder="Harga" type="text" name="harga" class="form-control mb-2" value="<?=$data['mobil']['harga']; ?>">
		    <a href="" class="badge badge-info">Ubah</a>
		    <a href="<?= BASEURL; ?>/mobil" class="card-link">Kembali</a>
		</div>
	</div>
</div>